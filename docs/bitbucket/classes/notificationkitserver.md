[php-sayhey-push-notification-kit](../README.md) › [Globals](../globals.md) › [NotificationKitServer](notificationkitserver.md)

# Class: NotificationKitServer

## Hierarchy

* **NotificationKitServer**

## Index

### Constructors

* [constructor](notificationkitserver.md#markdown-header-constructor)

### Properties

* [_initialized](notificationkitserver.md#markdown-header-private-_initialized)
* [apiKey](notificationkitserver.md#markdown-header-apikey)
* [apiVersion](notificationkitserver.md#markdown-header-apiversion)
* [appId](notificationkitserver.md#markdown-header-appid)
* [httpClient](notificationkitserver.md#markdown-header-private-httpclient)

### Methods

* [guard](notificationkitserver.md#markdown-header-private-guard)
* [pushNotifications](notificationkitserver.md#markdown-header-pushnotifications)
* [registerDeviceToken](notificationkitserver.md#markdown-header-registerdevicetoken)
* [unregisterDevice](notificationkitserver.md#markdown-header-unregisterdevice)

## Constructors

###  constructor

\+ **new NotificationKitServer**(`options`: object): *[NotificationKitServer](notificationkitserver.md)*

Defined in push-notification-kit.ts:41

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`apiVersion?` | undefined &#124; string |
`notificationApiKey` | string |
`notificationAppId` | string |
`notificationUrl` | string |

**Returns:** *[NotificationKitServer](notificationkitserver.md)*

## Properties

### `Private` _initialized

• **_initialized**: *boolean* = false

Defined in push-notification-kit.ts:14

___

###  apiKey

• **apiKey**: *string*

Defined in push-notification-kit.ts:8

___

###  apiVersion

• **apiVersion**: *string*

Defined in push-notification-kit.ts:12

___

###  appId

• **appId**: *string*

Defined in push-notification-kit.ts:10

___

### `Private` httpClient

• **httpClient**: *AxiosInstance*

Defined in push-notification-kit.ts:16

## Methods

### `Private` guard

▸ **guard**<**T**>(`cb`: function): *Promise‹Result‹T, [AppError](apperror.md)››*

Defined in push-notification-kit.ts:18

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

▸ (...`args`: any[]): *Promise‹T | [AppError](apperror.md)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [AppError](apperror.md)››*

___

###  pushNotifications

▸ **pushNotifications**(`params`: object): *Promise‹Result‹object, [AppError](apperror.md)››*

Defined in push-notification-kit.ts:91

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`androidImageUrl?` | undefined &#124; string |
`androidTag?` | undefined &#124; string |
`body?` | undefined &#124; string |
`category?` | undefined &#124; string |
`channelId?` | undefined &#124; string |
`data?` | undefined &#124; object |
`imageUrl?` | undefined &#124; string |
`iosImageUrl?` | undefined &#124; string |
`iosTag?` | undefined &#124; string |
`threadId?` | undefined &#124; string |
`title?` | undefined &#124; string |
`userIds` | string[] |

**Returns:** *Promise‹Result‹object, [AppError](apperror.md)››*

___

###  registerDeviceToken

▸ **registerDeviceToken**(`data`: object): *Promise‹Result‹object, [AppError](apperror.md)››*

Defined in push-notification-kit.ts:60

**Parameters:**

▪ **data**: *object*

Name | Type |
------ | ------ |
`instanceId` | string |
`token` | string |
`userId` | string |

**Returns:** *Promise‹Result‹object, [AppError](apperror.md)››*

___

###  unregisterDevice

▸ **unregisterDevice**(`instanceId`: string): *Promise‹Result‹object, [AppError](apperror.md)››*

Defined in push-notification-kit.ts:78

**Parameters:**

Name | Type |
------ | ------ |
`instanceId` | string |

**Returns:** *Promise‹Result‹object, [AppError](apperror.md)››*
