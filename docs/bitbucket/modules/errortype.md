[php-sayhey-push-notification-kit](../README.md) › [Globals](../globals.md) › [ErrorType](errortype.md)

# Module: ErrorType

## Index

### Enumerations

* [AuthErrorType](../enums/errortype.autherrortype.md)
* [DeviceTokenErrorType](../enums/errortype.devicetokenerrortype.md)
* [InternalErrorType](../enums/errortype.internalerrortype.md)
* [ServerErrorType](../enums/errortype.servererrortype.md)

### Type aliases

* [Type](errortype.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[InternalErrorType](../enums/errortype.internalerrortype.md) | [DeviceTokenErrorType](../enums/errortype.devicetokenerrortype.md) | [ServerErrorType](../enums/errortype.servererrortype.md) | [AuthErrorType](../enums/errortype.autherrortype.md)*

Defined in types.ts:57
