declare const _default: (config: {
    baseURL: string;
    apiKey: string;
}) => import("axios").AxiosInstance;
export default _default;
