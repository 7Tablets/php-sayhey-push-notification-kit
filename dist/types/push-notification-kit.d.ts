import { AppError } from './types';
import { Result } from 'neverthrow';
export default class NotificationKitServer {
    apiKey: string;
    appId: string;
    apiVersion: string;
    private _initialized;
    private httpClient;
    private guard;
    constructor(options: {
        notificationAppId: string;
        notificationApiKey: string;
        apiVersion?: string;
        notificationUrl: string;
    });
    registerDeviceToken(data: {
        userId: string;
        instanceId: string;
        token: string;
    }): Promise<Result<{
        status: number;
    }, AppError>>;
    unregisterDevice(instanceId: string): Promise<Result<{
        status: number;
    }, AppError>>;
    pushNotifications(params: {
        title?: string;
        body?: string;
        userIds: string[];
        data?: {
            [key: string]: string | number;
        };
        imageUrl?: string;
        androidImageUrl?: string;
        iosImageUrl?: string;
        channelId?: string;
        threadId?: string;
        category?: string;
        iosTag?: string;
        androidTag?: string;
    }): Promise<Result<{
        status: number;
    }, AppError>>;
}
