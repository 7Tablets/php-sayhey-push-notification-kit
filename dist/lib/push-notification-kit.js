"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var types_1 = require("./types");
var neverthrow_1 = require("neverthrow");
var constants_1 = require("./constants");
var axios_1 = require("./axios");
var NotificationKitServer = /** @class */ (function () {
    function NotificationKitServer(options) {
        this._initialized = false;
        var notificationApiKey = options.notificationApiKey, notificationAppId = options.notificationAppId, apiVersion = options.apiVersion, notificationUrl = options.notificationUrl;
        this.apiKey = notificationApiKey;
        this.appId = notificationAppId;
        this.apiVersion = apiVersion || constants_1.API_VERSION;
        this.httpClient = axios_1.default({
            baseURL: notificationUrl + "/" + this.apiVersion + "/apps/" + this.appId,
            apiKey: this.apiKey
        });
        this._initialized = true;
    }
    NotificationKitServer.prototype.guard = function (cb) {
        return __awaiter(this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._initialized) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.AppError('NotificationKit has not been initialized!', types_1.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof types_1.AppError) {
                            return [2 /*return*/, neverthrow_1.err(res)];
                        }
                        return [2 /*return*/, neverthrow_1.ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof types_1.AppError) {
                            return [2 /*return*/, neverthrow_1.err(e_1)];
                        }
                        return [2 /*return*/, neverthrow_1.err(new types_1.AppError('Unknown error', types_1.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    NotificationKitServer.prototype.registerDeviceToken = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guard(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, status;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.Register, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: data
                                        })];
                                case 1:
                                    status = (_b.sent()).status;
                                    return [2 /*return*/, { status: status }];
                            }
                        });
                    }); })];
            });
        });
    };
    NotificationKitServer.prototype.unregisterDevice = function (instanceId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guard(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, status;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.Unregister, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint(instanceId),
                                            method: Method
                                        })];
                                case 1:
                                    status = (_b.sent()).status;
                                    return [2 /*return*/, { status: status }];
                            }
                        });
                    }); })];
            });
        });
    };
    NotificationKitServer.prototype.pushNotifications = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, this.guard(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, status;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.SendNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, this.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: params
                                        })];
                                case 1:
                                    status = (_b.sent()).status;
                                    return [2 /*return*/, { status: status }];
                            }
                        });
                    }); })];
            });
        });
    };
    return NotificationKitServer;
}());
exports.default = NotificationKitServer;
//# sourceMappingURL=push-notification-kit.js.map