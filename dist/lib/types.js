"use strict";
/**
 * HTTP Methods
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppError = exports.ErrorType = exports.HttpMethod = void 0;
var HttpMethod;
(function (HttpMethod) {
    HttpMethod["Post"] = "post";
    HttpMethod["Get"] = "get";
    HttpMethod["Delete"] = "delete";
    HttpMethod["Patch"] = "patch";
    HttpMethod["Put"] = "put";
})(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
var ErrorType;
(function (ErrorType) {
    var AuthErrorType;
    (function (AuthErrorType) {
        AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
        AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
        AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
        AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
        AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
        AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
        AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
        AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
        AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
        AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
        AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
        AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
        AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
        AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
        AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
        AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
        AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
        AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
        AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
        AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
        AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
        AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
        AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
    })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
    var ServerErrorType;
    (function (ServerErrorType) {
        ServerErrorType["UnknownError"] = "UnknownError";
        ServerErrorType["InternalServerError"] = "InternalServerError";
    })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
    var DeviceTokenErrorType;
    (function (DeviceTokenErrorType) {
        DeviceTokenErrorType["InvalidPushParams"] = "DeviceTokenErrorTypeInvalidPushParams";
        DeviceTokenErrorType["AlreadyRegisteredForUser"] = "DeviceTokenErrorTypeAlreadyRegisteredForUser";
        DeviceTokenErrorType["NotFound"] = "DeviceTokenErrorTypeNotFound";
        DeviceTokenErrorType["NoTokensToPush"] = "DeviceTokenErrorTypeNoTokensToPush";
        DeviceTokenErrorType["OnlyStringData"] = "DeviceTokenErrorTypeOnlyStringData";
    })(DeviceTokenErrorType = ErrorType.DeviceTokenErrorType || (ErrorType.DeviceTokenErrorType = {}));
    var InternalErrorType;
    (function (InternalErrorType) {
        InternalErrorType["Uninitialized"] = "InternalUninitialized";
        InternalErrorType["Unknown"] = "InternalUnknown";
        InternalErrorType["AuthMissing"] = "InternalDetailsMissing";
    })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
var AppError = /** @class */ (function () {
    function AppError(err, errorType) {
        var _this = this;
        this.setErrorActions = function (params) {
            if (params === void 0) { params = {}; }
            var authFailed = params.authFailed, exchange = params.exchange;
            _this.exchange = exchange;
            _this.authFailed = authFailed;
        };
        this.setMessage = function (msg) {
            _this.message = msg;
        };
        this.setStatusCode = function (statusCode) {
            _this.statusCode = statusCode;
        };
        if (errorType) {
            this.errorType = errorType;
        }
        if (err instanceof Error) {
            this.error = err;
            this.message = err.message;
        }
        else {
            this.message = err;
        }
    }
    return AppError;
}());
exports.AppError = AppError;
//# sourceMappingURL=types.js.map