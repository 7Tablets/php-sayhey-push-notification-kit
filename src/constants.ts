import { HttpMethod } from './types'

export const API_VERSION = 'v1'

export const API_KEY_NAME = 'x-notification-server-api-key'

export const MESSAGE_READ_UPDATE_INTERVAL = 5000

export const Routes = {
  Register: {
    Endpoint: '/device-tokens',
    Method: HttpMethod.Post
  },
  Unregister: {
    Endpoint: (instanceId: string) => `/device-tokens/${instanceId}`,
    Method: HttpMethod.Delete
  },
  SendNotification: {
    Endpoint: '/push',
    Method: HttpMethod.Post
  }
}
